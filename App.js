/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Image,
  StatusBar,
  Button,
  Alert,
  TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import Carinfo from './src/component/atom/carinfo.js';

const App: () => React$Node = () => {
  let carname = 'Honda Tulip';
  let carSpec1 = {
    name : 'Ford Tulip',
    age : '17 bulan',
    belief : 'tulipisme',
    description : 'tulipku ada 5, rupa rupa warnanya. hijau kuning kelabu, merah muda dan biru. meletus tulip hijau. dor. hatiku sangat kacau. tulip kupegang erat. jangan sampai pecah lagi.',
    img : "../../image/lavender.jpg"
  };

  let carSpec2 = {
    name : 'Honda Tulip',
    age : '5 bulan',
    belief : 'roseisme',
    description : 'jalan jalan ke tanah deli. naik perahu dayung sendiri. kawan jangan bersedih, banyak tulip tumbuh disini.',
    img : "../../image/rose.jpg"
  };

  let carSpec3 = {
    name : 'Mercedes Tulip',
    age : '10 bulan',
    belief : 'Paprikaisme',
    description : 'askdjh askjdhaisu alskdjoawi aslkda. askjdh82. awod8 q29skamnd. q092uekjwqh askdj9q2n q938y3ej /qw.,ado',
    img : "../../image/tulip.jpg"
  };

  const alertButton = () => {
    Alert.alert('Previous Button pressed');
  }

  const alertButton2 = () => {
    Alert.alert('Next Button pressed');
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>

         
    
      <Carinfo spec={carSpec2}/>

    
      <ScrollView>
        <View style={styles.container}>
          <View style={{flex:1}}>
        <Button
              title="Previous" color="#f00faa" 
              onPress={() => (alertButton())}
            />
            </View>
            <View style={{flex:1}}> 
        <Button
            title="Next"
            onPress={() => (alertButton2())}
          />
          </View>
        </View>
      </ScrollView>

      
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  container:{
    flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  carname:{
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
