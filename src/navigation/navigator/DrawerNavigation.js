import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import App from '../../../App';
import { About } from '../../screens/About';

const Drawer = createDrawerNavigator();

export default function(props) {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name={'Home'} component={App}/>
            <Drawer.Screen name={'About'} component={About}/>
        </Drawer.Navigator>
    );
}